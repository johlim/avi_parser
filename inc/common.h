#ifndef COMMON_H
#define COMMON_H


#include "stdio.h"
#include "stdlib.h"


typedef signed char int8_t;
typedef unsigned char   uint8_t;
typedef short  int16_t;
typedef unsigned short  uint16_t;
typedef int  int32_t;
typedef unsigned   uint32_t;
//typedef long long  int64_t;
//typedef unsigned long long uint64_t;

#define RSHIFT(a,b) ((a) > 0 ? ((a) + ((1<<(b))>>1))>>(b) : ((a) + ((1<<(b))>>1)-1)>>(b))
/* assume b>0 */
#define ROUNDED_DIV(a,b) (((a)>0 ? (a) + ((b)>>1) : (a) - ((b)>>1))/(b))
#define FFABS(a) ((a) >= 0 ? (a) : (-(a)))
#define FFSIGN(a) ((a) > 0 ? 1 : -1)

#define FFMAX(a,b) ((a) > (b) ? (a) : (b))
#define FFMAX3(a,b,c) FFMAX(FFMAX(a,b),c)
#define FFMIN(a,b) ((a) > (b) ? (b) : (a))
#define FFMIN3(a,b,c) FFMIN(FFMIN(a,b),c)

#define FCC(ch4) (ch4[0] | ch4[1]<<8 | ch4[2]<<16 | ch4[3] << 24)
#define MKTAG(a,b,c,d) (a | (b << 8) | (c << 16) | (d << 24))
#define MKBETAG(a,b,c,d) (d | (c << 8) | (b << 16) | (a << 24))

enum CodecType {
    CODEC_TYPE_UNKNOWN = -1,
    CODEC_TYPE_VIDEO,
    CODEC_TYPE_AUDIO,
    CODEC_TYPE_DATA,
    CODEC_TYPE_SUBTITLE,
    CODEC_TYPE_ATTACHMENT,
    CODEC_TYPE_NB
};


typedef enum {
	LEVEL_NONE = 0x0,
	LEVEL_ERROR = 0x1,
	LEVEL_DEBUG = 0x2,
	LEVEL_TRACE = 0x3
} BL_DBG_LEVEL;

typedef enum {
	MODULE_NONE			= 0x0,
	MODULE_VIDEO		= 0x1,
	MODULE_AUDIO 		= 0x2,
	MODULE_DATA 		= 0x4,
	MODULE_SUBTITLE   	= 0x8,
	MODULE_ATTACHMENT   = 0x16,	
	MODULE_UI    		= 0x32,	
	MODULE_ALL			= 0xFFFF
} BL_DBG_MODULE;

void BL_Trace(const char* file, int line, const char* func, const int module, BL_DBG_LEVEL level, const char* format, ...);
const char * avi_hex_dump(const void *data, unsigned int len);

#define CLTRACE(M...)	{\
			BL_Trace(__FILE__,__LINE__,__func__,M);}
#endif
