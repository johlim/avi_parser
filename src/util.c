 /*

 \file   :   avi_parser_main.c
 \author :   jhlim
 \date   :   07/27/2016
 \brief  :   this file contains functions for txts stream extracting

 */

// http://www.alexander-noe.com/video/documentation/avi.pdf
// http://blog.naver.com/PostView.nhn?blogId=shlee7708&logNo=120121689464&redirect=Dlog&widgetTypeCall=true
/* Header file include --------------------------------------------------*/
#include "common.h"
#include "avstream.h"
/* Define  --------------------------------------------------------------*/

#define REGISTER_DEMUXER(X,x) { \
    extern AVInputFormat x##_demuxer; \
    av_register_input_format(&x##_demuxer); }


#define DETECT_SIZE (32*1024)
/* Define variable  -----------------------------------------------------*/
AVInputFormat *first_iformat = NULL;
/* Define extern variable & function  -----------------------------------*/
/* Function prototype  --------------------------------------------------*/
/* Function implementation  ---------------------------------------------*/
void av_register_input_format(AVInputFormat *format)
{
	AVInputFormat *p;
	CLTRACE(MODULE_UI, LEVEL_TRACE, "%s \r\n",__func__);
	
	p = first_iformat;

	if(p == NULL)
	{
		first_iformat = format;
		format->next = NULL;
	}
	else
	{
		while (p->next !=  NULL) p = p->next;
		 p->next = format;
		format->next = NULL;
	}
}

static AVInputFormat *av_probe_input_format(AVProbeData *pd)
{
	AVInputFormat *fmt1,*fmt;
	int score;
	int find = 0;
	CLTRACE(MODULE_UI, LEVEL_TRACE, "%s \r\n",__func__);
	fmt = NULL;

	if(first_iformat == NULL)
		printf("NULL is RRRRRRRRRRRRRR \r\n");

	for(fmt1 = first_iformat; fmt1 != NULL; fmt1 = fmt1->next) {
		if (fmt1->read_probe) {
			if(fmt1->read_probe(pd) > 0)
			{
				CLTRACE(MODULE_UI, LEVEL_TRACE, "%s fmt found\r\n",__func__);
				return fmt1;
			}
		} 
		
	}

	

	return fmt1;

}


int av_read_header_file(AVFormatContext *ic_ptr)
{
	int ret = -1;
	AVInputFormat *fmt = ic_ptr->iformat;

	if(fmt->read_header)
		ret = fmt->read_header(ic_ptr);

	return ret;
}

int av_open_input_file(AVFormatContext *ic_ptr)
{
	AVProbeData pd;
	AVInputFormat *fmt;
	char buf[DETECT_SIZE];

	int ret;

	ret = fileRead(ic_ptr->fp, buf, DETECT_SIZE);
	if(ret <= 0) return -1;

	if(fileSeek(ic_ptr->fp,0, SEEK_SET) < 0) return -1;
	
	pd.buf = buf;
	pd.buf_size = ret;
	
	fmt = av_probe_input_format(&pd);
	if(!fmt) return -1;

	ic_ptr->iformat = fmt;

	//printf("%08X \r\n", ic_ptr->iformat);
	
	return 0;
}

void av_register_all(void)
{
	REGISTER_DEMUXER(AVI, avi);
}

////////////////////////////////////////////
#include <stdarg.h>
#include <time.h> 

#define DEBUG_LENGTH	512

static int g_DebugLevel = -1;	//CL_DEBUG
static int g_DebugModule = MODULE_ALL;
/**
 *  \brief Brief
 *  
 *  \param [in] index Parameter_Description
 *  \param [in] enable Parameter_Description
 *  \return Return_Description
 *  
 *  \details Details
 */
static long getMSTime(void)
{
	struct timespec now;

	clock_gettime(CLOCK_MONOTONIC, &now);

	return (now.tv_sec*1000) + (now.tv_nsec/1000000);
}
/**
 *  \brief Brief
 *  
 *  \param [in] index Parameter_Description
 *  \param [in] enable Parameter_Description
 *  \return Return_Description
 *  
 *  \details Details
 */
void BL_Trace(const char* file, int line, const char* func, const int module, BL_DBG_LEVEL level, const char* format, ...)
{
	
	va_list args;
	char* genv = NULL;
	char str[DEBUG_LENGTH] = {0};
	char *filename = NULL;

	if(g_DebugLevel == -1)
	{
		genv = getenv("BL_DEBUG");
		if(genv)
		{
			if(strcmp(genv, "LEVEL_NONE") == 0)
			{
				g_DebugLevel = LEVEL_NONE;
			}
			else if(strcmp(genv, "LEVEL_ERROR") == 0)
			{
				g_DebugLevel = LEVEL_ERROR;
			}
			else if(strcmp(genv, "LEVEL_DEBUG") == 0)
			{
				g_DebugLevel = LEVEL_DEBUG;
			}
			else if(strcmp(genv, "LEVEL_TRACE") == 0)
			{
				g_DebugLevel = LEVEL_TRACE;
			}
			else
			{
				g_DebugLevel = LEVEL_ERROR;
			}
		}
		else
		{
			g_DebugLevel = LEVEL_ERROR;
		}
	}

	if(!g_DebugLevel || (int)level > g_DebugLevel)
		return;

	if (!(module & g_DebugModule))
		return;

	filename = strrchr(file, '/')+1;
	
	//if(!filename)
		filename = file;
	
	if(level == LEVEL_TRACE)
		snprintf(str, DEBUG_LENGTH, "%08ld : [TRACE] %s : %d line (%s) : ", getMSTime(), filename, line, func);
	else if(level == LEVEL_DEBUG)
		snprintf(str, DEBUG_LENGTH, "%08ld : [DEBUG] %s : %d line (%s) : ", getMSTime(), filename, line, func);
	else if(level == LEVEL_ERROR)
		snprintf(str, DEBUG_LENGTH, "%08d : [ERROR] %s : %d line (%s) : ", getMSTime(), filename, line, func);
		
	va_start(args, format);
	vsprintf(str+(strlen(str)), format, args);
	va_end(args);

	printf("%s", str);
}
/**
 *  \brief Brief
 *  
 *  \param [in] index Parameter_Description
 *  \param [in] enable Parameter_Description
 *  \return Return_Description
 *  
 *  \details Details
 */
const char *
avi_hex_dump(const void *data, unsigned int len)
{
	static char string[1024] = {0};
	unsigned char *d = (unsigned char *) data;
	unsigned int i;

	for (i = 0; len--; i += 3) {
		if (i >= sizeof(string) - 4)
			break;
		snprintf(string+i, 4, "%02x ", *d++);
	}

	string[i] = '\0';
	return string;
}