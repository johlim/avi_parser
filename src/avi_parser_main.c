/*

 \file   :   avi_parser_main.c
 \author :   jhlim
 \date   :   07/27/2016
 \brief  :   this file contains functions for txts stream extracting

*/

// http://www.alexander-noe.com/video/documentation/avi.pdf
// http://blog.naver.com/PostView.nhn?blogId=shlee7708&logNo=120121689464&redirect=Dlog&widgetTypeCall=true
/* Header file include --------------------------------------------------*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h> 

#include "avi.h"

#include "avstream.h"
#include "common.h"

/* Define  --------------------------------------------------------------*/
#define VERSION "0.0.1"

/* Define variable  -----------------------------------------------------*/
char infile[128];
char outfile[128];
int debug_print;
int dump_out;

/* Define extern variable & function  -----------------------------------*/
AVFormatContext avContext;

/* Function prototype  --------------------------------------------------*/
void print_help();

/* Function implementation  ---------------------------------------------*/

/**
 *  \brief Brief
 *  
 *  \param [in] index Parameter_Description
 *  \param [in] enable Parameter_Description
 *  \return Return_Description
 *  
 *  \details Details
 */
int main(int argc, char **argv)
{
	int c;
	FILE *fp;
	
	if (argc == 1)
	{
		print_help();
		exit(1);
	}
	while(( c = getopt(argc, argv, "hvdf:o:")) != -1)
		switch (c)
		{
			case 'h':
				print_help();
				exit(0);
				break;
			case 'v':
				printf("avi_parser %s\n", VERSION);
				exit(0);
			case 'd':
				debug_print = 1;
				break;
			case 'f':
				sprintf(infile,"%s", optarg);
				break;
			case 'o':
				sprintf(outfile,"%s", optarg);
				dump_out=1;
				break;
			case '?':
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);
				print_help();
				exit(1);
		}

		
	fp = fopen(infile, "rb");
	if(fp == NULL) 
	{
		perror("error \r\n");
		return -1;
	}

	avContext.fp = fp;
	av_register_all();

	if(av_open_input_file(&avContext)	 < 0)
	{
		printf("can't detect file format \r\n");
		goto FAIL;	
	}

	CLTRACE(MODULE_UI,LEVEL_TRACE,"find file format \r\n");
	CLTRACE(MODULE_UI,LEVEL_TRACE,"fmt address : %08X \r\n", avContext.iformat);
	if(av_read_header_file(&avContext) < 0)
	{
		printf("incorrect header format \r\n");
		goto FAIL;
	}


	if(avContext.formatType == AVI_FORMAT)
	{
		int i;
		
		printf("AVI FILE Format \r\n");
		AVIStream *st = (AVIStream *)avContext.filecontain;
		AVIStreamHeader *stheader;
		char *s,*s1;

		printf("stream count : %d, Size: %ld \r\n", st->nbStreams, st->aviSize);
		printf("MaxBytesPerSec : %d, MicroSecPerFrame : %d \r\n", st->mainHeader.dwMaxBytesPerSec, st->mainHeader.dwMicroSecPerFrame);

		for(i=0; i < st->nbStreams; i++)
		{
			stheader = &(st->streamHeader[i]);

			s = (char *)&(stheader->fccType);
			s1 = (char *)&(stheader->fccHandler);
			printf("fccType : %c%c%c%c, fccHandler : %c%c%c%c \r\n", s[0],s[1],s[2],s[3], s1[0],s1[1],s1[2],s1[3]);
			printf("rate : %d, Scale: %d,  %f \r\n", stheader->dwRate , stheader->dwScale, (float)(stheader->dwRate/stheader->dwScale));
		}
		
	}
	else
	{
		printf("Unknown FILE Format \r\n");
	}
FAIL:
	fclose(fp);

	return 0;
}

/**
 *  \brief Brief
 *  
 *  \param [in] index Parameter_Description
 *  \param [in] enable Parameter_Description
 *  \return Return_Description
 *  
 *  \details Details
 */
void print_help(void)
{
	printf("avi_parser (ver.%s) extract txts stream file AVI file\n", VERSION);
	printf("avi_parser [-h] [-v] [-f FILE] \n\n");
	printf(" -h print this help and exit\n");
	printf(" -v print version and exit\n");
	printf(" -f FILE set file \n");
}
